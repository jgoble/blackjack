# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
    XDG_CACHE_HOME: "$CI_PROJECT_DIR/.cache"

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
    paths:
        - .cache/pip

stages:
    - unit test
    - lint
    - complexity
    - type check
    - type check strict
    - build dist
    - check wheel
    - check sdist
    - pypi test upload
    - check pypi test
    - pypi release
    - gitlab release

.python:
    image: registry.gitlab.com/jgoble/docker-images/ci/blackjack:latest
    before_script:
        - python -VV  # extended version information
    needs: []
    variables:
        PIP_ROOT_USER_ACTION: ignore

.nogit:
    extends: .python
    variables:
        GIT_STRATEGY: none

.test:
    extends: .python
    except:
        - tags

unit test:
    extends: .test
    stage: unit test
    script:
        - pip install -e .
        - pytest
    coverage: '/TOTAL[\s\d]+?(\d{1,3}\.\d{2})%/'
    artifacts:
        paths:
            - coverage.xml
            - htmlcov/*

lint:
    extends: .test
    stage: lint
    script:
        - pip install -r requirements.txt
        - pylint src/

type check strict:
    extends: .test
    stage: type check strict
    script:
        - pip install -r requirements.txt
        - mypy --strict src/

build dist:
    extends: .python
    stage: build dist
    script:
        - python -m build
    artifacts:
        paths:
            - dist/*

.check:
    extends: .nogit
    needs:
        - job: build dist
          artifacts: true

check wheel:
    extends: .check
    stage: check wheel
    script:
        - cd dist
        - pip install pyblackjack*.whl
        - pip uninstall -y pyblackjack

check sdist:
    extends: .check
    stage: check sdist
    script:
        - cd dist
        - pip install pyblackjack*.tar.gz
        - pip uninstall -y pyblackjack

.upload:
    extends: .nogit
    only:
        - tags
    script:
        - twine upload --repository-url "$CI_ENVIRONMENT_URL" --username __token__ --password "$PYPI_API_TOKEN" dist/*

pypi test upload:
    extends: .upload
    stage: pypi test upload
    needs:
        - job: build dist
          artifacts: true
    environment:
        name: pypi-test
        url: https://test.pypi.org/legacy/

check pypi test:
    extends: .nogit
    stage: check pypi test
    only:
        - tags
    needs:
        - job: pypi test upload
          artifacts: false
    script:
        - pip install --index-url https://test.pypi.org/simple/ --no-deps pyblackjack==${CI_COMMIT_TAG#v}
        - pip uninstall -y pyblackjack

pypi release:
    extends: .upload
    stage: pypi release
    needs:
        - job: build dist
          artifacts: true
        - job: check pypi test
          artifacts: false
    when: manual
    allow_failure: false
    environment:
        name: pypi-real
        url: https://upload.pypi.org/legacy/

gitlab release:
    stage: gitlab release
    image: registry.gitlab.com/gitlab-org/release-cli:latest
    needs:
        - job: pypi release
          artifacts: false
    only:
        - tags
    variables:
        GIT_STRATEGY: none
    script: echo "Publishing release to GitLab..."
    release:
        tag_name: $CI_COMMIT_TAG
        name: Release $CI_COMMIT_TAG
        # Eventually will do a proper changelog
        description: "Download now!"
        assets:
            links:
                - name: Binary wheel (universal)
                  url: https://pypi.debian.net/pyblackjack/pyblackjack-${CI_COMMIT_TAG#v}-py3-none-any.whl
                  filepath: /wheel
                - name: Source distribution (gzipped tarball)
                  url: https://pypi.debian.net/pyblackjack/pyblackjack-${CI_COMMIT_TAG#v}.tar.gz
                  filepath: /sdist
